package com.example.smartdiet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/* DOA class for food database. Author: Hieu Tran */
public class FoodDatabase {
	
	private SQLiteDatabase db;
	private FoodDatabaseHelper foodDB_open;
	private String[] allColumns = { FoodDatabaseHelper.COLUMN_ID,
			FoodDatabaseHelper.COLUMN_NAME, FoodDatabaseHelper.COLUMN_GROUP, FoodDatabaseHelper.COLUMN_CALORIES };
	
	public FoodDatabase(Context context){
		foodDB_open = new FoodDatabaseHelper(context);
	}
	
	/* Open the food database */
	public boolean open(){
		try{
			db = foodDB_open.getWritableDatabase();
			return true;
		}catch(SQLException e){
			return false;
		}
	}
	
	/* Close the food database */
	public void close(){
		foodDB_open.close();
	}
	
	/* Insert new food into the database methods */
	public void insertFood(Food food){
		ContentValues fvalues = new ContentValues();
		fvalues.put(FoodDatabaseHelper.COLUMN_NAME, food.getName());
		fvalues.put(FoodDatabaseHelper.COLUMN_CALORIES, food.getCalories());
		fvalues.put(FoodDatabaseHelper.COLUMN_GROUP, food.getCalories());
		
		db.insert(FoodDatabaseHelper.FOODS, null, fvalues);
	}
	
	public long insertFood(String name, int calories, String group){
		ContentValues fvalues = new ContentValues();
		fvalues.put(FoodDatabaseHelper.COLUMN_NAME, name);
		fvalues.put(FoodDatabaseHelper.COLUMN_CALORIES, calories);
		fvalues.put(FoodDatabaseHelper.COLUMN_GROUP, group);
		
		return db.insert(FoodDatabaseHelper.FOODS, null, fvalues);
	}
	
	/* Delete foods from database methods */
	public boolean deleteFood(Food f){
		String name = f.name;
		try{
			db.delete(FoodDatabaseHelper.FOODS, FoodDatabaseHelper.COLUMN_NAME + " = " + name, null);
			return true;
		}catch(SQLException e){
			return false;
		}
	}
	
	public boolean deleteFood(String name){
		try{
			db.delete(FoodDatabaseHelper.FOODS, FoodDatabaseHelper.COLUMN_NAME + " = " + name, null);
			return true;
		}catch(SQLException e){
			return false;
		}
	}
	
	public boolean deleteFood(long id){
		try{
			db.delete(FoodDatabaseHelper.FOODS, FoodDatabaseHelper.COLUMN_NAME + " = " + String.valueOf(id), null);
			return true;
		}catch(SQLException e){
			return false;
		}
	}
	
	/* Get all of the foods from the database */
	public List<String> getAllFoods(){
		List<String> food_list = new ArrayList<String>();
		
		Cursor c = db.query(FoodDatabaseHelper.FOODS,
		        allColumns, null, null, null, null, null);
		
		System.out.println(c.getCount());
		c.moveToFirst();
		int i = 1;
		while(!c.isAfterLast()){
			System.out.println("getting food # " + String.valueOf(i));
			String food = c.getString(1);
			food_list.add(food);
			c.moveToNext();
			i++;
		}
		
		return food_list;
	}
	
	/* Get all of the foods from a specified group */
	public List<String> getFoodsByGroup(String group){
		List<String> food_list = new ArrayList<String>();
		
		Cursor c = db.rawQuery("select " + FoodDatabaseHelper.COLUMN_NAME + " from " + FoodDatabaseHelper.FOODS + 
								" where " + FoodDatabaseHelper.COLUMN_GROUP + " = ? ;", new String[] { group });
		
		c.moveToFirst();
		while(!c.isAfterLast()){
			String food = c.getString(0);
			food_list.add(food);
			c.moveToNext();
		}
		
		return food_list;
	}
	
	/* Get all of the foods within the specified range of calories */
	public List<String> getFoodsByCalories(int min_cal, int max_cal){
		List<String> food_list = new ArrayList<String>();
		
		Cursor c = db.rawQuery("select " + FoodDatabaseHelper.COLUMN_NAME + " from " + FoodDatabaseHelper.FOODS + 
					" where " + FoodDatabaseHelper.COLUMN_CALORIES + " >= ? AND " +
					FoodDatabaseHelper.COLUMN_CALORIES + " <= ? ;", new String[] { String.valueOf(min_cal), String.valueOf(max_cal) });
		
		c.moveToFirst();
		while(!c.isAfterLast()){
			String food = c.getString(0);
			food_list.add(food);
			c.moveToNext();
		}
		
		return food_list;
	}
	
	/* Get food by unique name */
	public Food getFoodByName(String name){
		Cursor c = db.rawQuery("select * from " + FoodDatabaseHelper.FOODS + 
					" where " + FoodDatabaseHelper.COLUMN_NAME + " = ? ;", new String[] { name });
		c.moveToFirst();
		return cursorToFood(c);
	}
	
	
	/* Method to parse the cursor to Food object */
	public Food cursorToFood(Cursor c){
		Food food = new Food();
		food.setName(c.getString(1));
		food.setGroup(c.getString(2));
		food.setCalories(c.getInt(3));
		return food;
	}
	
	

}
