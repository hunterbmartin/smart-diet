package com.example.smartdiet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import android.content.ContentValues;
import android.content.Context;
import android.database.*;
import android.database.sqlite.*;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Path;
import android.provider.MediaStore.Files;
import android.util.Log;

/* Class to create and open a food database. Author: Hieu Tran */
public class FoodDatabaseHelper extends SQLiteOpenHelper {
	
	/* Food table attributes */
	public static final String FOODS = "foods";
	public static final String COLUMN_ID = "rowid";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_GROUP = "food_group";
	public static final String COLUMN_CALORIES = "kcal";
	
	/*Servings table attributes */
	public static final String SERVINGS = "servings";
	public static final String COLUMN_FNAME = "name";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_SIZE = "size";
	
	private static String DB_PATH = "";
	private static final String DB_NAME = "food_db";
	private static final String SQL_FILE_NAME = "food_db.sql";
	private static int version = 1;
	
	private Context helperContext;
	SQLiteDatabase db;
	
	public FoodDatabaseHelper(Context context) {
		super(context, DB_NAME, null, version);
		
		if(android.os.Build.VERSION.SDK_INT >= 17){
			DB_PATH = context.getApplicationInfo().dataDir + "/databases/";         
		}
		else
		{
			DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
		}
		this.helperContext = context;
		
		db = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		System.out.println("Creating..h");
		try{
			copySQL();
			Log.v("DB_HELPER", "SQL file copied!");
		}catch(IOException e){
			Log.e("DB_HELPER", "Error copying SQL file");
		}
				
		try {
			execSQLFile(DB_PATH + SQL_FILE_NAME);
		} catch (IOException e) {
			Log.e("DB_HELPER", "Error reading sql file!");
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		db.execSQL("drop table if exists foods");
		db.execSQL("drop table if exists servings");
		db.execSQL("drop table if eixsts meals");
		
		System.out.println("Creating..h");
		try{
			copySQL();
			Log.v("DB_HELPER", "SQL file copied!");
		}catch(IOException e){
			Log.e("DB_HELPER", "Error copying SQL file");
		}
				
		try {
			execSQLFile(DB_PATH + SQL_FILE_NAME);
		} catch (IOException e) {
			Log.e("DB_HELPER", "Error reading sql file!");
		}
		
	}
	
	
	/* Change the version of the database */
	public void setVersion(int newVersion){
		version = newVersion;
	}
	
	/*
	 * Copy the SQL query file
	 */
	private void copySQL() throws IOException{
		InputStream in = helperContext.getAssets().open(SQL_FILE_NAME);
		String outFileName = DB_PATH + SQL_FILE_NAME;
		OutputStream out = new FileOutputStream(outFileName);
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) > 0){
			out.write(buffer, 0, read);
		}
		out.flush();
		out.close();
		in.close();
	}
	
	/*
	 * Check if database already exists in android path
	 */
	private boolean checkSQL(){
		File f = new File(DB_PATH + SQL_FILE_NAME);
		return f.exists();
	}
	
	/* Read a SQL file in a String */
	private void execSQLFile( String file ) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
		   System.out.println("line");
		   db.execSQL(line);
		}
		br.close();
	}
	
}
